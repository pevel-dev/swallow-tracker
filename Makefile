all: build down up

up:
	docker compose up -d

down:
	docker compose down --remove-orphans

build:
	docker compose build

lint:
	pre-commit run --all-files

test:
	# docker compose run --rm api poetry run pytest
	# docker compose run --rm telegram_client pytest

dev:
	docker compose run --rm --volume=${PWD}/api:/api --publish 8000:8000 api bash -c 'poetry run uvicorn src.main:app --host 0.0.0.0 --port 8000 --reload'

dev_test:
	docker compose run --rm --volume=${PWD}/api:/api api poetry run pytest

pre-commit-install:
	pip install pre-commit
	pre-commit install
	pre-commit install --hook-type commit-msg
