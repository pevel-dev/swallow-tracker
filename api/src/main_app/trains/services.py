import httpx
from src.logger import logger
from src.main_app.trains.domains import Train
from src.main_app.trains.dto import SerializedTrain


class TrainService:
    @staticmethod
    async def get_train_info(train_id, station1, station2, date) -> Train:
        url = 'http://mobile.svrpk.ru/v3.1/api.php?action=train_info&result_type=json'
        headers = {'Host': 'mobile.svrpk.ru', 'Content-Type': 'application/x-www-form-urlencoded'}

        request = f'key_client=ecd9da47594fe253124dfb2fb862891b\
        &ppk_id=1\
        &trainId={train_id}\
        &st1={station1}\
        &st2={station2}\
        &date={date}'

        async with httpx.AsyncClient() as client:
            answer = await client.post(url, content=request, headers=headers)

        result = answer.json()
        logger.info(
            f'Request external API\nurl:{url}\ndata:{request}\nStatus code: {answer.status_code}\nResult:{result}'
        )

        train = Train(SerializedTrain(result['TrainInfo']['@attributes']))
        if isinstance(result['TrainInfo']['Carriage'], dict):
            result['TrainInfo']['Carriage'] = [result['TrainInfo']['Carriage']]
        carriages = result['TrainInfo']['Carriage']
        total_free_places_count = 0
        for index, carriage in enumerate(carriages):
            current_free_places_count = int(carriage['@attributes']['PlaceCount'])
            total_free_places_count += current_free_places_count

        stations = result['TrainInfo']['Station']
        station_from = stations[0]['@attributes']
        station_to = stations[-1]['@attributes']
        train.departing_time = station_from['Dep']
        train.arrival_time = station_to['Arr']
        train.free_places = total_free_places_count
        return train

    @staticmethod
    async def get_train_list_by_route(station1, station2, date) -> list[Train]:
        url = 'http://mobile.svrpk.ru/v3.1/api.php?action=train_list_by_range&result_type=json'
        headers = {'Host': 'mobile.svrpk.ru', 'Content-Type': 'application/x-www-form-urlencoded'}
        data = {
            'ppk_id': '1',
            'sort': 'dep',
            'count': '200',
            'st1': station1,
            'st2': station2,
            'date': date,
            'key_client': 'ecd9da47594fe253124dfb2fb862891b',
        }

        async with httpx.AsyncClient() as client:
            answer = await client.post(url, data=data, headers=headers)

        result = answer.json()
        logger.info(f'Request external API\nurl:{url}\ndata:{data}\nStatus code: {answer.status_code}\nResult:{result}')

        if not isinstance(result['Schedule'], dict) or 'Train' not in result['Schedule']:
            return []

        if isinstance(result['Schedule']['Train'], dict):
            result['Schedule']['Train'] = [result['Schedule']['Train']]

        trains = []
        for train in result['Schedule']['Train']:
            dto = SerializedTrain(train['@attributes'])
            if dto.Type == '3':
                trains.append(Train(dto))

        return trains
