from src.main_app.trains.dto import SerializedTrain


class Train:
    id: str
    number: str
    name: str
    departing_time: str
    arrival_time: str
    station_from_id: str
    station_to_id: str
    free_places: int

    def __init__(self, serialized_train: SerializedTrain):
        self.id = serialized_train.Id
        self.number = serialized_train.Num
        self.name = serialized_train.Name
        self.departing_time = serialized_train.Dep
        self.arrival_time = serialized_train.Arr
        self.station_from_id = serialized_train.StationFrom
        self.station_to_id = serialized_train.StationTo
        self.free_places = int(serialized_train.PlaceCount) if serialized_train.PlaceCount else 0
