class SerializedTrain:
    O: str = None
    Id: str = None
    Num: str = None
    Name: str = None
    Type: str = None
    SubType: str = None
    Dep: str = None
    Arr: str = None
    IsTimeDef: str = None
    Sc: str = None
    Ex: str = None
    StationFrom: str = None
    StationTo: str = None
    PlaceCount: str = None

    def __init__(self, in_dict: dict):
        for key, val in in_dict.items():
            if isinstance(val, (list, tuple)):
                setattr(self, key, [SerializedTrain(x) if isinstance(x, dict) else x for x in val])
            else:
                setattr(self, key, SerializedTrain(val) if isinstance(val, dict) else val)
