from fastapi import APIRouter, Depends
from fastapi.encoders import jsonable_encoder
from src.logger import logger
from src.main_app.trains.services import TrainService
from src.main_app.trains.transport.di import get_train_service
from src.main_app.trains.transport.requests import TrainListRequest
from src.pkg.responses import SuccessOut
from src.stations import stations
from starlette.responses import JSONResponse

trains_router = APIRouter(prefix='/trains', tags=['Trains'])


@trains_router.post(path='/list_by_route', responses={200: {'model': SuccessOut}})
async def get_train_list(
    request: TrainListRequest,
    train_service: TrainService = Depends(get_train_service),
) -> JSONResponse:
    station1_id = stations[request.dep_station]
    station2_id = stations[request.arr_station]
    trains = await train_service.get_train_list_by_route(station1_id, station2_id, request.route_date)
    result = jsonable_encoder(trains)
    logger.info(f'Call endpoint /api/trains/list_by_route | data: {request}\nResult:{result}')

    if not result:
        return JSONResponse(None, status_code=404)

    return JSONResponse(result)
