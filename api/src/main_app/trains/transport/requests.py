from pydantic import AfterValidator, BaseModel
from src.pkg.validators import known
from typing_extensions import Annotated


class TrainListRequest(BaseModel):
    user_id: int | None = None
    dep_station: Annotated[str, AfterValidator(known)] | None = None
    arr_station: Annotated[str, AfterValidator(known)] | None = None
    route_date: str | None = None
