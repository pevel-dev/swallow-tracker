from datetime import date

from src.stations import stations
from src.worker.services import WorkerService


class TrackQueueService:
    def __init__(self, worker_service: WorkerService):
        self._worker_service = worker_service

    async def put_on_queue(self, user_id: int, train_id: int, station1: str, station2: str, route_date: date) -> int:
        station1_id = stations.get(station1)
        station2_id = stations.get(station2)
        async with self._worker_service:
            job = await self._worker_service.send_task(
                'check_track',
                user_id=user_id,
                train_id=train_id,
                station1_id=station1_id,
                station2_id=station2_id,
                route_date=route_date,
            )
            return job.job_id
