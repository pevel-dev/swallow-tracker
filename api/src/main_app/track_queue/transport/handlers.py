from fastapi import APIRouter, Depends
from src.logger import logger
from src.main_app.track_queue.services import TrackQueueService
from src.main_app.track_queue.transport.di import get_track_queue_service
from src.main_app.track_queue.transport.requests import PutTrackIn
from src.pkg.responses import SuccessOut
from starlette.responses import JSONResponse

track_router = APIRouter(prefix='/track', tags=['Track'])


@track_router.post(path='', responses={200: {'model': SuccessOut}})
async def add_track_to_queue(
    track_request: PutTrackIn,
    track_queue_service: TrackQueueService = Depends(get_track_queue_service),
) -> JSONResponse:
    job_id = await track_queue_service.put_on_queue(
        track_request.user_id,
        track_request.train_id,
        track_request.station1,
        track_request.station2,
        track_request.route_date,
    )
    logger.info(f'Call endpoint /api/track | data: {track_request}\nResult:job_id={job_id}')
    return SuccessOut(job_id=job_id)
