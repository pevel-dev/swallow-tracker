from fastapi import Depends
from src.main_app.track_queue.services import TrackQueueService
from src.worker.di import get_worker_service
from src.worker.services import WorkerService


def get_track_queue_service(worker_service: WorkerService = Depends(get_worker_service)) -> TrackQueueService:
    return TrackQueueService(worker_service)
