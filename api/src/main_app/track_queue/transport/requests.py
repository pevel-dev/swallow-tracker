from datetime import date
from typing import Annotated

from pydantic import AfterValidator, BaseModel
from src.pkg.validators import known


class PutTrackIn(BaseModel):
    user_id: int
    train_id: int
    station1: Annotated[str, AfterValidator(known)] | None = None
    station2: Annotated[str, AfterValidator(known)] | None = None
    route_date: str
