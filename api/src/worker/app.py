from arq.connections import RedisSettings
from src.config import settings
from src.main_app.trains.transport.di import get_train_service
from src.worker.tasks import check_track


async def startup(ctx):
    # Setup dependencies
    # ctx['logger'] = logger.logger
    ctx['TrainService'] = get_train_service()
    pass


async def shutdown(ctx):
    pass


class WorkerSettings:
    redis_settings = RedisSettings(host=settings.REDIS_HOST, port=settings.REDIS_PORT)
    functions = [check_track]
    on_startup = startup
    on_shutdown = shutdown
    max_tries = 10**9
    max_jobs = 2048
