import arq
from arq.jobs import Job


class WorkerService:
    def __init__(self, host: str, port: int):
        self._host = host
        self._port = port
        self._job_pool: arq.ArqRedis | None = None

    async def _make_job_pool(self):
        settings = arq.connections.RedisSettings(host=self._host, port=self._port)
        return await arq.create_pool(settings)

    async def send_task(self, task_name: str, *args, **kwargs) -> Job:
        return await self._job_pool.enqueue_job(task_name, *args, **kwargs)

    async def __aenter__(self):
        self._job_pool = await self._make_job_pool()
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        if self._job_pool is not None:
            await self._job_pool.aclose()
