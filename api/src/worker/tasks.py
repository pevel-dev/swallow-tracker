import json

import arq
import httpx
import loguru
from src import config
from src.main_app.trains.services import TrainService
from src.stations import stations


async def check_track(
    ctx: dict,
    user_id: int,
    train_id: int,
    station1_id: int,
    station2_id: int,
    route_date: str,
):
    train_service: TrainService = ctx['TrainService']
    train_info = await train_service.get_train_info(
        train_id,
        station1_id,
        station2_id,
        route_date,
    )
    loguru.logger.info(train_info)
    station_1 = None
    station_2 = None
    for key in stations:
        if stations.get(key) == station1_id:
            station_1 = key
        if stations.get(key) == station2_id:
            station_2 = key
    data = {
        'user_id': user_id,
        'train_id': train_id,
        'station1': station_1,
        'station2': station_2,
        'dep_time': train_info.departing_time,
        'arr_time': train_info.arrival_time,
        'free_places': train_info.free_places,
        'date': route_date,
    }
    try:
        async with httpx.AsyncClient() as client:
            answer = await client.post(
                f'http://telegram_client:{config.settings.CALLBACK_API_PORT}', content=json.dumps(data)
            )
            if answer.status_code == 404:
                return
            if answer.status_code == 200:
                if train_info.free_places > 10:
                    raise arq.Retry(defer=3600)
                raise arq.Retry(defer=60)
    except httpx.HTTPError as _:
        raise arq.Retry(defer=120)
