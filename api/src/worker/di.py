from src.config import settings
from src.worker.services import WorkerService


async def get_worker_service() -> WorkerService:
    return WorkerService(settings.REDIS_HOST, settings.REDIS_PORT)
