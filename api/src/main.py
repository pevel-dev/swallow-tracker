from fastapi import APIRouter, FastAPI
from src.config import settings
from src.logger import logger
from src.main_app.track_queue.transport.handlers import track_router
from src.main_app.trains.transport.handlers import trains_router

app = FastAPI(title='SwallowTracker', debug=settings.DEBUG)

api_router = APIRouter(prefix='/api')
api_router.include_router(track_router)
api_router.include_router(trains_router)

app.include_router(api_router)
