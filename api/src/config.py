from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')
    TELEGRAM_TOKEN_NOTIFIER: str | None
    CHAT_ID_NOTIFIER: int | None
    CHAT_ID_2_NOTIFIER: int | None
    REDIS_HOST: str | None
    REDIS_PORT: int | None
    DEBUG: bool = False
    CALLBACK_API_PORT: int | None


settings = Settings(_env_file='.env')
