from src.stations import stations


def known(station):
    assert station.lower() in stations
    return station.lower()
