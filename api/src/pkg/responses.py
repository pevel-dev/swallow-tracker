from pydantic import BaseModel


class SuccessOut(BaseModel):
    job_id: str


class ErrorOut(BaseModel):
    status: int
    message: str
