import json
import time

import config
import models
from aiogram import Bot
from aiogram.fsm.storage.base import StorageKey
from aiogram.fsm.storage.redis import DefaultKeyBuilder, RedisStorage
from aiohttp import web
from callback_api.worker_handler.requests import ApiRequest
from loguru import logger
from redis.asyncio import Redis

bot = Bot(config.settings.TELEGRAM_TOKEN)
storage = RedisStorage(redis=Redis(host=config.settings.REDIS_HOST, port=config.settings.REDIS_PORT))


async def handle(request):
    request_content = (await request.content.read()).decode('UTF-8')
    request = ApiRequest.model_validate(json.loads(request_content), from_attributes=True)
    storage_key = StorageKey(bot_id=None, chat_id=request.user_id, user_id=request.user_id)
    tracks = (await storage.get_data(storage_key)).get('tracks')

    if tracks:
        new_tracks = []
        track = None
        for i in tracks:
            if i['train_id'] == str(request.train_id) and i['date'] == request.date:
                track = models.Track.model_validate(i, from_attributes=True)
            else:
                new_tracks.append(i)
        logger.info(tracks)
        logger.info(track)
        logger.info(request)

        if not track:
            return web.Response(status=404)
        if track.free_seats != request.free_places:
            dep = ' '.join(el.capitalize() for el in track.dep_station.split())
            arr = ' '.join(el.capitalize() for el in track.arr_station.split())
            text = f'''\
Отправление
{track.date}
{dep} - {arr}
({track.dep_time} - {track.arr_time})
 
Свободных мест: {track.free_seats} → {request.free_places}'''

            await bot.send_message(
                chat_id=request.user_id,
                text=text,
            )

        track.free_seats = request.free_places
        track.last_update_at = int(time.time())
        new_tracks.append(track.model_dump())
        data = await storage.get_data(storage_key)
        data['tracks'] = new_tracks
        await storage.set_data(storage_key, data)
    return web.Response(status=200)
