from pydantic import BaseModel


class ApiRequest(BaseModel):
    user_id: int
    train_id: int
    station1: str
    station2: str
    dep_time: str
    arr_time: str
    free_places: int
    date: str
