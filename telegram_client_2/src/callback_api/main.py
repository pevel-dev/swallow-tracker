import config
from aiohttp import web
from callback_api.worker_handler.handle import handle


async def run_aiohttp():
    app = web.Application()
    app.router.add_post('', handle)
    runner = web.AppRunner(app)
    await runner.setup()
    site = web.TCPSite(runner, '0.0.0.0', config.settings.CALLBACK_API_PORT)
    await site.start()
