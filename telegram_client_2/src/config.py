from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')
    TELEGRAM_TOKEN: str | None
    API_INTERNAL_PORT: str | None
    REDIS_HOST: str | None
    REDIS_PORT: int | None
    CALLBACK_API_PORT: int | None
    TELEGRAM_TOKEN_NOTIFIER: str | None
    CHAT_ID_NOTIFIER: int | None
    CHAT_ID_2_NOTIFIER: int | None


settings = Settings(_env_file='.env')
