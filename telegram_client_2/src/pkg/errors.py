from enum import StrEnum


class ErrorTypes(StrEnum):
    UNKNOWN_STATION = 'unknown_station'
    API_ERROR = 'api_error'
    NO_TRAINS = 'no_trains'
