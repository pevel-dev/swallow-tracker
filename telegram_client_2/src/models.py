from pydantic import BaseModel


class Track(BaseModel):
    date: str | None = None
    dep_station: str | None = None
    arr_station: str | None = None
    train_id: str | None = None
    last_update_at: int | None = None
    free_seats: int = 0
    task_id: str | None = None
    dep_time: str | None = None
    arr_time: str | None = None
