import config
import httpx
from loguru import logger
from pkg.errors import ErrorTypes


class ApiClient:
    async def get_trains(self, data: dict) -> tuple[dict, None] | tuple[None, ErrorTypes]:
        try:
            async with httpx.AsyncClient() as client:
                resp = await client.post(
                    f'http://api:{config.settings.API_INTERNAL_PORT}/api/trains/list_by_route',
                    json=data,
                )
                logger.info(
                    f'Request list_by_route | data: {data}.\nStatus Code: {resp.status_code}\nContent: {resp.content}'
                )
                if resp.status_code == 404:
                    return None, ErrorTypes.NO_TRAINS
                if resp.status_code == 422:
                    return None, ErrorTypes.UNKNOWN_STATION
                if resp.status_code != 200:
                    return None, ErrorTypes.API_ERROR

                return resp.json(), None

        except httpx.RequestError as error:
            logger.error(f'Request list_by_route | data: {data}.\nError: {error}')
            return None, ErrorTypes.API_ERROR

    async def new_track(self, data: dict) -> tuple[dict, None] | tuple[None, ErrorTypes]:
        try:
            async with httpx.AsyncClient() as client:
                resp = await client.post(
                    f'http://api:{config.settings.API_INTERNAL_PORT}/api/track',
                    json=data,
                )
                logger.info(
                    f'Request new_track | data: {data}.\nStatus Code: {resp.status_code}\nContent: {resp.content}'
                )
                if resp.status_code != 200:
                    return None, ErrorTypes.API_ERROR
                return resp.json(), None
        except httpx.RequestError as error:
            logger.error(f'Request new_track | data: {data}.\nError: {error}')
            return None, ErrorTypes.API_ERROR
