from api.client import ApiClient
from api.service import ApiService


def get_api_service() -> ApiService:
    return ApiService(api_client=ApiClient())
