from pkg.errors import ErrorTypes

from api.client import ApiClient


class ApiService:
    def __init__(self, api_client: ApiClient):
        self._client = api_client

    async def get_trains_from_api(
        self,
        user_id: int,
        dep_station: str,
        arr_station: str,
        route_date: str,
    ) -> tuple[dict, None] | tuple[None, ErrorTypes]:
        data = {
            'user_id': user_id,
            'dep_station': dep_station,
            'arr_station': arr_station,
            'route_date': route_date,
        }
        return await self._client.get_trains(data)

    async def new_track(
        self,
        user_id: int,
        train_id: str,
        station1: str,
        station2: str,
        route_date: str,
    ) -> tuple[dict, None] | tuple[None, ErrorTypes]:
        data = {
            'user_id': user_id,
            'train_id': train_id,
            'station1': station1,
            'station2': station2,
            'route_date': route_date,
        }
        return await self._client.new_track(data)
