from datetime import datetime, timedelta, timezone

import models
from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message
from loguru import logger
from pkg.errors import ErrorTypes
from telegram import states
from telegram.static import keyboards, texts
from telegram.static.callbacks import CUSTOM_DATE_CALLBACK, TODAY_CALLBACK, TOMORROW_CALLBACK
from telegram.static.keyboards import trains_kbd

from api.di import get_api_service
from api.service import ApiService

new_track_router = Router()


async def set_track(state: FSMContext, data: models.Track) -> None:
    await state.set_data({'new_track': dict(data.model_dump()), 'tracks': (await state.get_data()).get('tracks')})


async def get_track(state: FSMContext) -> models.Track:
    data = (await state.get_data())['new_track']
    return models.Track.model_validate(data, from_attributes=True)


@new_track_router.message(F.text == texts.START_MENU_BUTTON_NEW_TRACK)
async def start_new_track(message: Message, state: FSMContext):
    await state.set_state(states.NewTrack.choosing_date)
    await set_track(state, models.Track())
    await message.answer(text=texts.CHOOSE_DATE, reply_markup=keyboards.CHOOSE_DATE_KBD)


@new_track_router.callback_query(states.NewTrack.choosing_date, F.data == TODAY_CALLBACK)
async def choose_date_today(query: CallbackQuery, state: FSMContext):
    track = await get_track(state)
    track.date = datetime.now(tz=timezone(timedelta(hours=5))).date().strftime('%d.%m.%Y')

    await set_track(state, track)
    await state.set_state(states.NewTrack.choose_dep_station)
    await query.message.delete_reply_markup()
    await query.message.answer(text=texts.CHOOSE_DEP_STATION)


@new_track_router.callback_query(states.NewTrack.choosing_date, F.data == TOMORROW_CALLBACK)
async def choose_date_tomorrow(query: CallbackQuery, state: FSMContext):
    track = await get_track(state)
    track.date = (datetime.now(tz=timezone(timedelta(hours=5))).date() + timedelta(1)).strftime('%d.%m.%Y')

    await set_track(state, track)
    await state.set_state(states.NewTrack.choose_dep_station)
    await query.message.delete_reply_markup()
    await query.message.answer(text=texts.CHOOSE_DEP_STATION)


@new_track_router.callback_query(states.NewTrack.choosing_date, F.data == CUSTOM_DATE_CALLBACK)
async def choose_date_custom_date_answer(query: CallbackQuery, state: FSMContext):
    await state.set_state(states.NewTrack.choosing_custom_date)
    await query.message.delete_reply_markup()
    await query.message.answer(texts.CHOOSE_DATE_CUSTOM_ANSWER)


@new_track_router.message(states.NewTrack.choosing_custom_date)
async def choose_date_custom_date(message: Message, state: FSMContext):
    try:
        date = datetime.strptime(message.text, '%d.%m.%Y').strftime('%d.%m.%Y')
    except ValueError as exp:
        logger.error(exp)
        await message.answer(texts.INCORRECT_CUSTOM_DATE)
        return
    track = await get_track(state)
    track.date = date
    await set_track(state, track)
    await state.set_state(states.NewTrack.choose_dep_station)
    await message.answer(texts.CHOOSE_DEP_STATION)


@new_track_router.message(states.NewTrack.choose_dep_station)
async def choose_dep_station(message: Message, state: FSMContext):
    if len(message.text.strip()) < 3:
        await message.answer(texts.INCORRECT_STATION)
        return
    track = await get_track(state)
    track.dep_station = message.text.lower()
    await set_track(state, track)
    await state.set_state(states.NewTrack.choose_arr_station)
    await message.answer(texts.CHOOSE_ARR_STATION)


@new_track_router.message(states.NewTrack.choose_arr_station)
async def choose_arr_station(message: Message, state: FSMContext, api_service: ApiService = get_api_service()):
    if len(message.text.strip()) < 3:
        await message.answer(texts.INCORRECT_STATION)
        return
    track = await get_track(state)
    track.arr_station = message.text.lower()
    await set_track(state, track)
    await state.set_state(states.NewTrack.choosing_train)

    trains, error = await api_service.get_trains_from_api(
        message.from_user.id,
        track.dep_station,
        track.arr_station,
        track.date,
    )

    if error:
        match error:
            case ErrorTypes.UNKNOWN_STATION:
                await message.answer(texts.INCORRECT_ROUTE)
                await state.set_state(states.NewTrack.choose_dep_station)
                await message.answer(text=texts.CHOOSE_DEP_STATION)
            case ErrorTypes.API_ERROR:
                await message.answer(texts.API_ERROR, reply_markup=keyboards.MENU_KBD)
                await state.set_state(None)
            case ErrorTypes.NO_TRAINS:
                await message.answer(texts.NO_TRAINS, reply_markup=keyboards.MENU_KBD)
                await state.set_state(None)
        return
    await message.answer(texts.CHOOSE_TRAIN, reply_markup=trains_kbd(trains))


@new_track_router.callback_query(states.NewTrack.choosing_train)
async def choose_train(
    query: CallbackQuery,
    state: FSMContext,
    api_service: ApiService = get_api_service(),
):
    track = await get_track(state)
    track.train_id, track.dep_time, track.arr_time = query.data.split('_')[1:]

    await set_track(state, track)

    await query.message.delete_reply_markup()
    job, error = await api_service.new_track(
        query.from_user.id, track.train_id, track.dep_station, track.arr_station, track.date
    )
    if error:
        await query.message.answer(texts.API_ERROR)
        await state.set_state(None)
        return

    track.task_id = job['job_id']
    tracks = (await state.get_data()).get('tracks')
    if tracks is None:
        tracks = []
    for i in tracks:
        if track.train_id == i['train_id'] and track.date == i['date']:
            await query.message.answer(texts.TRACK_EXISTED)
            await state.set_state(None)
            return

    tracks.append(track.model_dump())
    logger.info(tracks)
    await state.update_data({'tracks': tracks})
    await query.message.answer(texts.TRACK_CREATED, reply_markup=keyboards.MENU_KBD)
    await state.set_state(None)
