import time

import pymorphy3
from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.types import CallbackQuery, Message

import models
from telegram.static import texts
from telegram.static.keyboards import delete_from_tracks_list, morph_for_seats, tracks_list_kbd

track_list_router = Router()


@track_list_router.message(F.text == texts.START_MENU_BUTTON_MY_TRACKS)
async def track_list(message: Message, state: FSMContext):
    tracks = (await state.get_data()).get('tracks')
    if tracks is None or len(tracks) == 0:
        await message.answer(texts.NOT_TRACKS)
        return
    await message.answer(texts.YOUR_TRACKS, reply_markup=tracks_list_kbd(tracks))


morph = pymorphy3.MorphAnalyzer()
morph_second = morph.parse('секунда')[0]


@track_list_router.callback_query(lambda call: 'check' in call.data)
async def track_list_item(query: CallbackQuery, state: FSMContext):
    track_job_id = query.data.split('_')[-1]
    tracks = (await state.get_data()).get('tracks')
    track = None
    for i in tracks:
        if i.get('task_id') == track_job_id:
            track = models.Track.model_validate(i, from_attributes=True)
    if track:
        await query.message.delete()

        last_seen = 'никогда'
        if track.last_update_at is not None:
            seconds_word = morph_second.make_agree_with_number(int(round(time.time() - track.last_update_at, 0))).word
            last_seen = f'{int(round(time.time() - track.last_update_at, 0))} {seconds_word} назад'

        dep = ' '.join(el.capitalize() for el in track.dep_station.split())
        arr = ' '.join(el.capitalize() for el in track.arr_station.split())
        places_word = morph_for_seats.make_agree_with_number(track.free_seats).word
        text = f'''\
{track.date}
{dep} - {arr}
({track.dep_time} - {track.arr_time})
свободно {track.free_seats} {places_word}

Последнее обновление: {last_seen}'''

        await query.message.answer(text, reply_markup=delete_from_tracks_list(track))


@track_list_router.callback_query(lambda call: 'stop' in call.data)
async def track_list_delete_item(query: CallbackQuery, state: FSMContext):
    track_job_id = query.data.split('_')[-1]
    tracks = (await state.get_data()).get('tracks')
    new_tracks = []
    for i in tracks:
        if i.get('task_id') != track_job_id:
            new_tracks.append(i)
    await state.set_data({'tracks': new_tracks})

    await query.message.delete()
    await track_list(query.message, state)
