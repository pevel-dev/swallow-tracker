from aiogram import F, Router
from aiogram.fsm.context import FSMContext
from aiogram.types import Message
from telegram.static import keyboards, texts

default_router = Router()


@default_router.message(F.text == '/start')
async def start(message: Message, state: FSMContext):
    await state.set_state(None)
    await message.answer(texts.START, reply_markup=keyboards.MENU_KBD)


@default_router.message(F.text == texts.START_MENU_BUTTON_HELP)
async def help(message: Message):
    await message.answer(texts.HELP)


@default_router.message()
async def not_found(message: Message):
    await message.answer(texts.NOT_FOUND)
