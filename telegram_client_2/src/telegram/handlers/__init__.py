from telegram.handlers.default import default_router
from telegram.handlers.new_track import new_track_router
from telegram.handlers.track_list import track_list_router

router_list = [
    track_list_router,
    new_track_router,
    default_router,
]
