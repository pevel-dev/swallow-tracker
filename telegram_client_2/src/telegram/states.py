from aiogram.fsm.state import State, StatesGroup


class NewTrack(StatesGroup):
    choosing_date = State()
    choosing_custom_date = State()
    choose_dep_station = State()
    choose_arr_station = State()
    choosing_train = State()
