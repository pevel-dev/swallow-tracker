import config
from aiogram import Bot, Dispatcher
from aiogram.fsm.storage.redis import RedisStorage
from redis.asyncio.client import Redis
from telegram import handlers


async def start_bot():
    dp = Dispatcher(storage=RedisStorage(redis=Redis(host=config.settings.REDIS_HOST, port=config.settings.REDIS_PORT)))

    dp.include_routers(*handlers.router_list)

    bot = Bot(config.settings.TELEGRAM_TOKEN)
    await dp.start_polling(bot)
