import pymorphy3
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup

import models
from telegram.static import texts
from telegram.static.callbacks import CUSTOM_DATE_CALLBACK, TODAY_CALLBACK, TOMORROW_CALLBACK

NEW_TRACK_BTN = KeyboardButton(text=texts.START_MENU_BUTTON_NEW_TRACK)
MY_TRACKS_BTN = KeyboardButton(text=texts.START_MENU_BUTTON_MY_TRACKS)
HELP_BTN = KeyboardButton(text=texts.START_MENU_BUTTON_HELP)

MENU_KBD = ReplyKeyboardMarkup(
    keyboard=[
        [NEW_TRACK_BTN, MY_TRACKS_BTN],
        [HELP_BTN]
    ],
    resize_keyboard=True,
    input_field_placeholder=texts.START_MENU_PLACEHOLDER
)

TODAY_BTN = InlineKeyboardButton(text=texts.CHOOSE_DATE_TODAY, callback_data=TODAY_CALLBACK)
TOMORROW_BTN = InlineKeyboardButton(text=texts.CHOOSE_DATE_TOMORROW, callback_data=TOMORROW_CALLBACK)
CUSTOM_DATE_BTN = InlineKeyboardButton(text=texts.CHOOSE_DATE_CUSTOM, callback_data=CUSTOM_DATE_CALLBACK)
CHOOSE_DATE_KBD = InlineKeyboardMarkup(
    inline_keyboard=[
        [TODAY_BTN, TOMORROW_BTN, CUSTOM_DATE_BTN]
    ]
)

morph = pymorphy3.MorphAnalyzer()
morph_for_seats = morph.parse('место')[0]


def trains_kbd(trains: dict):
    # FIXME: Обработать случай, если кнопок больше чем разрешенное количество строк клавиатуры (пагинация)
    buttons = []
    for train in trains:
        places_word = morph_for_seats.make_agree_with_number(train["free_places"]).word
        text = f'{train["departing_time"]} - {train["arrival_time"]} ({train["free_places"]} {places_word}) | {train["name"]}'
        btn = [InlineKeyboardButton(
            text=text,
            callback_data=f'id_{train["id"]}_{train["departing_time"]}_{train["arrival_time"]}')]
        buttons.append(btn)

    return InlineKeyboardMarkup(inline_keyboard=buttons)


def tracks_list_kbd(tracks: list[dict]):
    # FIXME: Обработать случай, если кнопок больше чем разрешенное количество строк клавиатуры (пагинация)
    buttons = []
    for track in tracks:
        track = models.Track.model_validate(track, from_attributes=True)
        dep = ' '.join(el.capitalize() for el in track.dep_station.split())
        arr = ' '.join(el.capitalize() for el in track.arr_station.split())
        text = f'{track.date} | {track.dep_time} - {track.arr_time} | {dep} - {arr}'

        buttons.append([InlineKeyboardButton(text=text, callback_data=f'check_{track.task_id}')])

    return InlineKeyboardMarkup(inline_keyboard=buttons)


def delete_from_tracks_list(track: models.Track):
    buttons = [InlineKeyboardButton(text='❌ Не отслеживать', callback_data=f'stop_{track.task_id}')]
    return InlineKeyboardMarkup(inline_keyboard=[buttons])
