import asyncio

from callback_api.main import run_aiohttp
from telegram.main import start_bot

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    tasks = [loop.create_task(start_bot()), loop.create_task(run_aiohttp())]
    loop.run_until_complete(asyncio.gather(*tasks))
